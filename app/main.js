express = require('express');
const cors = require('cors');

app = express();

app.use(cors());

app.get('/', function(req, res) {
    res.send('Hello World');
});

app.listen(31250, '0.0.0.0', function() {
    console.log('Example app listening on port 31250!');
});